import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/ordermanagement', params);

// 查询
export const queryAPI = params => req('get', '/ordermanagement', params);

// 修改
export const updateAPI = params => req('put', '/ordermanagement', params);

// 删除
export const deleteAPI = params => req('delete', '/ordermanagement/' + params);



